var gulp = require('gulp');
var gutil = require('gulp-util');
var del = require('del');
var nodemon = require('gulp-nodemon');
var zip = require('gulp-zip');

gulp.task('clean', function() {
	return del(['build/**/*', 'build/.*']);
});

gulp.task('cleanPackage', function() {
	return del(['package/**/*']);
});

gulp.task('copy', ['copyPackageJson'], function() {
	return gulp.src(['source/**/*', 'source/.*/**/*']).pipe(gulp.dest('build'));
});

gulp.task('copyPackageJson', function() {
	return gulp.src('package.json').pipe(gulp.dest('build'));
});

gulp.task('package', ['clean', 'cleanPackage', 'copy'], function () {
	return gulp.src(['build/**/*', 'build/.*/**/*']).pipe(zip('package.zip')).pipe(gulp.dest('package'))
})

gulp.task('default', ['clean', 'copy'], function() {
	nodemon({
		script: "./build/app.js",
		env: { "NODE_ENV": "development",
			   "port" : "8080" }
	}).on("restart", function() {
		console.log("nodemon restarted server.js");	
	})
});