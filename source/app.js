// Dependencies
var express = require('express');
var http = require('http')
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var errorHandler = require('errorhandler');
var multer = require('multer');

var app = express();

var routes = require('./routes')
var authHelper = require('./routes/helpers/authHelper');
var authentication = require('./routes/authentication');
var message = require('./routes/messages');

// App config
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
// TODO: put favicon if available
// app.use(express.favicon());

app.use(logger('dev'));
app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({ secret: 'This is secret', resave: true, saveUninitialized: true }));
app.use(express.static(path.join(__dirname, 'public')));

if(process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'test') {
    app.use(errorHandler({ dumpExceptions: true, showStack: true }));
}

// Routes
// Restrict access: must be logged in
function restrict(req, res, next) {
  // if the user is authenticated...
  if (req.session.authenticated) {
    // PROCEEED
    next();
  } else {
    // not authenticated - go to the back of the line
    res.redirect('/login');
  }
}

// Allows cross-domain scripting
app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, apikey');
  next();
});

app.options('*', function(req, res) {
  res.status(200).send("Ok");
});

app.get('/', authentication.login);
// app.get('/login', authentication.login);
// app.get('/home', restrict, routes.home);
// app.get('/logout', restrict, authentication.logout);
// app.get('/addUser', restrict, authentication.addUser);
// app.get('/modelData', routes.modelData);

app.get('/authenticate', authentication.authenticate);

// TODO: Disabled for now since it may pose security threat
// app.get('/getHaveItemComments', authHelper.restrictByApiKey, inspection.getHaveItemComments);
// app.get('/getMessageById/:id', authHelper.restrictByApiKey, message.getMessageById);
// app.get('/getBase64MessageById/:id', authHelper.restrictByApiKey, message.getBase64MessageById);

// TODO: This is to accomodate some of the platform that can only support post endpoint
// app.post('/getHaveItemComments', authHelper.restrictByApiKey, inspection.getHaveItemComments);

// app.post('/getServiceCompletion', authHelper.restrictByApiKey, authHelper.getCustomerInformation, serviceCompletion.getServiceCompletion)
app.post('/uploadMessage', authHelper.restrictByApiKey, authHelper.getCustomerInformation, multer({ dest: './uploads/'}).single('file'), message.uploadMessage);
app.post('/getMessages', message.getMessageByDate);
// app.post('/getMobilePendingWorkOrder', authHelper.restrictByApiKey, workOrder.getMobilePendingWorkOrder);


http.createServer(app).listen(process.env.PORT, function(){
  console.log("Express server listening on port " + process.env.PORT);
});