function isLocalStorageEmpty() {
	return (localStorage.selections==undefined);
}

function resetLocalStorageArray() {
	var selections = [];
	localStorage.selections = JSON.stringify(selections);

	resetTable();
}

function addRowToLocalArray(data) {
	if (!isLocalStorageEmpty()) {
		var JSONData = localStorage.selections;
		var StringData = JSON.parse(JSONData);

		var item = { 
			id: StringData.length,
			item: 'Hose Fitting',
			item_id: data
		};

		StringData.push(item);
       	localStorage.selections = JSON.stringify(StringData);
	}
	else {
		console.log("Error trying to use uninitialised array");
	}
}

function getLocalArray() {
	if (!isLocalStorageEmpty()) {
		var JSONData = localStorage.selections;
		var StringData = JSON.parse(JSONData);
		return StringData;
	}
	else {
		return undefined;
	}
}

function resetTable() {
	// $('#myTable tr:last').after('<tr><td>' + data + '</td><td>Hose Fitting</td><td>1</td></tr>');       	
	$("#myTable tbody").remove();

}

function addTableRow(row) {

	// $('#myTable tr').after('<tr><td>' + 'test01' + '</td><td>Hose Fitting</td><td>1</td></tr>'); 

	var id = row.id;
	var item = row.item;
	var item_id = row.item_id;

	if (id != undefined && item != undefined && item_id != undefined) {
		//$('#myTable tr:last').after('<tr><td>' + id + '</td><td>Hose Fitting</td><td>1</td></tr>'); 
	}
	else {
		console.log("JSON not formatted correctly (missing attribute)");
	}

	// $('#myTable tr:last').after('<tr><td>' + data + '</td><td>Hose Fitting</td><td>1</td></tr>');       	

}
function setTableDisplay() {
	// var rows = localStorage.selections
	// rows.forEach(function(row) {
	// 	console.log("Row..");
	// });
	// resetTable();

	$('#myTable tr:last').after('<tr><td>' + 'test01' + '</td><td>Hose Fitting</td><td>1</td></tr>'); 


	var rows = getLocalArray();
	for (var i = 0; i < rows.length; i++) {
		console.log("row.."  + i );
		addTableRow(rows[i]);
	}
}



$(function(){
	// $('#tracker').html(hello);
	$("#btnResponder").click( function() {
		resetLocalStorageArray();
	});

	$("tableAdd").click( function() {

	});

	$("tableRemove").click( function() {

	});

	$("tableClear").click( function() {

	});



	$('#search').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search: $(this).val() };
       $.get( '/searching',parameters, function(data) {
		
       // $('#results').html(data);

       // if (localStorage.selections != undefined) {

       // 	var JSONData = localStorage.selections;
       // 	var StringData = JSON.parse(JSONData);
       // 	StringData.push(data);

       // 	localStorage.selections = JSON.stringify(StringData);

       // }

       // console.log(JSON.parse(localStorage.selections));

       // if (!isLocalStorageEmpty()) {

       // }

       addRowToLocalArray(data);
       setTableDisplay();

       // var selections = JSON.parse(localStorage.selections);
       // selections.append(data);
       // localStorage.selections = JSON.stringify(selections);

     });
       // Reset field
    $("#search").val("");
    };
 });
});


$(function() {
	// Focus on search field
	$("#search").focus();

	// Init
	if(isLocalStorageEmpty()) {
		resetLocalStorageArray();
	}
});