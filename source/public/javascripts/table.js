function clearTable() {
	$("#itemTable > tbody").html("");
}

function isLocalStorageEmpty() {
	return (localStorage.selections==undefined);
}

function resetLocalStorageArray() {
	var selections = [];
	localStorage.selections = JSON.stringify(selections);
}
function clearArray() {
	clearTable();
	resetLocalStorageArray();
}

function getLocalArray() {
	if (!isLocalStorageEmpty()) {
		var JSONData = localStorage.selections;
		var StringData = JSON.parse(JSONData);
		return StringData;
	}
	else {
		return undefined;
	}
}


function pushToArray(data) {
	if (!isLocalStorageEmpty()) {
		var StringData = getLocalArray();

		// Check if item already exists (Qty update)
		var found = false;
		var item;
		for (var i = 0; i < StringData.length; i++) {
			item = StringData[i];
			if (item != undefined && item.item_id != undefined && item.item_id == data) {
				item.qty = item.qty + 1;
				StringData[i] = item;
				found = true;
				console.log("Found!")
				break;
			}
		}
		if (!found) {
			console.log("Not found");
			item = { 
				id: StringData.length,
				item_id: data,
				item: 'Hose Fitting',
				qty: 1
			};
			StringData.push(item);
		}

       	localStorage.selections = JSON.stringify(StringData);
	}
	else {
		console.log("Error trying to use uninitialised array");
	}
}

function addTableRow(row) {
	var id = row.id;
	var item = row.item;
	var item_id = row.item_id;
	var qty = row.qty;

	if (id != undefined && item != undefined && item_id != undefined && qty != undefined) {
		var newRowContent = "<tr>"
		+ "<td>" + id + "</td>" 
		+ "<td>" + item_id + "</td>" 
		+ "<td>" + item + "</td>" 
		+ "<td>" + qty + "</td>" 
		+ "<td>" + "<button class=\"btn btn-default minusButton\" type=\"button\">-</button>" + "<button class=\"btn btn-default plusButton\" type=\"button\">+</button>" + "</td>"
		+ "</tr>";

		$("#itemTable tbody").append(newRowContent); 
	}
	else {
		console.log("JSON not formatted correctly (missing attribute)");
	}

}

function showTable() {
	// Clear old table
	clearTable();

	// Generate new one
	var items = getLocalArray();
	for (var i = 0; i < items.length; i++) {
		addTableRow(items[i]);
	}
}

// On page load
$(function() {
	console.log("Page loaded");

	// Focus on search field
	$("#search").focus();

	// Init storage
	if(isLocalStorageEmpty()) {
		resetLocalStorageArray();
	}
	else {
		showTable();
	}

	// Barcode Entry Field
	$("#search").on("keyup", function(keyEvent) {
		var params = { search: $(this).val() };
		if (keyEvent.keyCode == 13) {
			$.get('/searching', params, function(dataPassed) {
				pushToArray(dataPassed);
				
				showTable();

			});

			// Reset search field
			$("#search").val("");
		}
	});
});

function updateQtyForItem(item_id, qty) {
	if (!isLocalStorageEmpty()) {
		var StringData = getLocalArray();

		for (var i = 0; i < StringData.length; i++) {
			var item = StringData[i];
			if (item != undefined && item.item_id != undefined && item.item_id == item_id) {
				var current_qty = item.qty;
				if ((current_qty + qty) >= 1) {
					item.qty = item.qty + qty;
					StringData[i] = item;
					break;
				}
				else {
					StringData.splice(i,1);
				}
			}
		}

		localStorage.selections = JSON.stringify(StringData);
	}
}

var newRowContent = "<tr><td>t1</td><td>t2</td></tr>";
$("#tableAdd").click( function() {

	$("#itemTable tbody").append(newRowContent);
});

$(document).on('click', '.plusButton', function() {
	var item_id = $(this).closest("tr").find("td:nth-child(2)").text();
	updateQtyForItem(item_id, +1);
	showTable();
});

$(document).on('click', '.minusButton', function() {
	var item_id = $(this).closest("tr").find("td:nth-child(2)").text();
	updateQtyForItem(item_id, -1);
	showTable();
});


$("#tableClear").click( function() {
	clearArray();

	// Show alert
	$(".alert").show();
	$(".alert").fadeOut(3000 );
});