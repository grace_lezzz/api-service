var prodEnv = require('./prod_env.js');
var devEnv = require('./deve_env.js');
var schoolEnv = require('./school_env.js');

var configRHAUApp = devEnv.getConfigRHAUApp();
var configRHAUM3 = devEnv.getConfigRHAUM3();

var sql = require('mssql'); 
var environment = process.env.NODE_ENV;

var configFactoryM3;

// Database settings
// Environment determines database 
if (environment == 'development') {
	configRHAUApp = devEnv.getConfigRHAUApp();
	configRHAUM3 = devEnv.getConfigRHAUM3();
    configFactoryM3 = devEnv.getConfigFactoryM3();
}
else if (environment == 'production') {
	configRHAUApp = prodEnv.getConfigRHAUApp();
	configRHAUM3 = prodEnv.getConfigRHAUM3();
    configFactoryM3 = prodEnv.getConfigFactoryM3()
}
else if (environment == 'school') {
	configRHAUApp = schoolEnv.getConfigRHAUApp();
	configRHAUM3 = schoolEnv.getConfigRHAUM3();
    configFactoryM3 = schoolEnv.getConfigFactoryM3()
}	
else {
	// No environment set
	throw new Error("Environment not set");
}

var connectionRHAUApp = new sql.Connection(configRHAUApp, function(err) { 
    if (err) { 
		console.log("Database connection error: " + err.message);
		throw new Error("Could not connect to database (env = " + environment +")"); 
	}
});

var connectionRHAUM3 = new sql.Connection(configRHAUM3, function(err) { 
    if (err) { 
		console.log("Database connection error: " + err.message);
		throw new Error("Could not connect to database (env = " + environment +")"); 
	}
});

exports.getConnRHAUApp = function () {
	return connectionRHAUApp;
}

exports.getConnRHAUM3 = function () {
	return connectionRHAUM3;
}

exports.getConnectionFactoryM3 = function() {
    return configFactoryM3;
}

exports.getDBName = function () {
    if (environment == 'production') {
        return m3DbNamesProdFactory;
    } else {
        return m3DbNamesDeveFactory;
    }
}

exports.getEnvironment = function() {
    if(environment == 'production') {
        return "PROD";
    } else {
        return "DEVE";
    }
}

var m3DbNamesDeveFactory = function(customer_no) {
    switch(customer_no) {
        // Australia and Asia Pacific
        case 100: return 'RHAU_M3_Deve';
        // US
        case 300: return 'RHAE_M3_Deve';
        // Others        
        default: return 'RHAU_M3_Deve';
    }    
}

var m3DbNamesProdFactory = function(customer_no) {
    switch(customer_no) {
        // Australia and Asia Pacific
        case 100: return 'RHAU_M3_Prod';
        // US
        case 300: return 'RHAE_M3_Prod';
        // Others        
        default: return 'RHAU_M3_Prod';
    }    
}
