var sql = require('mssql'); 

var environment = process.env.NODE_ENV;

var configRHAUApp = {
    user: 'ryco',
    password: 'ryco1921',
    server: 'RycoVM01',
    database: "RHAU_Application",
    connectionTimeout: 300000,
    requestTimeout: 300000,
    pool: {
        idleTimeoutMillis: 300000,
        max: 100
    }
}

var configRHAUM3 = {
    user: 'ryco',
    password: 'ryco1921',
    server: 'RycoVM01',
    database: "RHAU_M3_Deve",
    connectionTimeout: 300000,
    requestTimeout: 300000,
    pool: {
        idleTimeoutMillis: 300000,
        max: 100
    }
}

var configRHAEM3 = {
    user: 'ryco',
    password: 'ryco1921',
    server: 'RycoVM01',
    database: "RHAE_M3_Deve",
    connectionTimeout: 300000,
    requestTimeout: 300000,
    pool: {
        idleTimeoutMillis: 300000,
        max: 100
    }
}

var connectionRHAUM3 = new sql.Connection(configRHAUM3, function(err) { 
    if (err) { 
		console.log("Db connection error: " + err.message);
		throw new Error("Could not connect to database (env = " + environment +")"); 
	}
});

var connectionRHAEM3 = new sql.Connection(configRHAEM3, function(err) { 
    if (err) { 
		console.log("Db connection error: " + err.message);
		throw new Error("Could not connect to database (env = " + environment +")"); 
	}
});

exports.getConfigRHAUApp = function () {
	return configRHAUApp;
}

exports.getConfigRHAUM3 = function () {
	return configRHAUM3;
}

exports.getConfigFactoryM3 = function() {
    return function(customer_no) {
        switch(customer_no) {
            // AU Customer
            case 100: return connectionRHAUM3;
            // US Customer
            case 300: return connectionRHAEM3;
            // Otherwise default to AU Customer
            default: return connectionRHAUM3;
        }
    }
}
