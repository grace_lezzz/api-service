// Author: MFreeman
// Date: 17/09/15
//
// Predefined messages and response codes for consistent feedback to applications

exports.get = function() {
	return {
		noUserFound: {
			error: true,
			error_code: 500,
			message: 'Please re-login to continue'
		},
		couldNotConnect: {
			error: true,
			message: 'Could not connect to database'
		},
		assetNotFound: {
			error: true,
			message: 'Asset not found'
		},
		validAsset: {
			error: false
		},
		modelNotFound: {
			error: true,
			error_code: 400,
			message: 'Configuration not found'
		},
		noDataSendToServer:{
			message: 'No data send to server'
		},
		assignMachineFailed:{
			message: 'Could not create a new assign machine'
		},
		assignMachineSuccess:{
			error: false,
			message: 'Assign machine has been recorded'
		},
		assignMachineError:{
			message: 'Error creating assign machine: '
		},
		invalidUsername:{
			message: 'Invalid username'
		},
		invalidApikey:{
			message: 'Invalid apikey'
		},
		invalidPassword:{
			message: 'Invalid password'
		},
		invalidContactname:{
			message: 'Invalid contact name'
		},
		invalidContactPhone:{
			message: 'Invalid contact phone'
		},
		invalidContactEmail:{
			message: 'Invalid contact email'
		},
		invalidCompany:{
			message: 'Invalid company'
		},
		userExist:{
			message: 'Username already exists. Please choose an alternative'
		},
		userCratedAndReviewed:{
			message: 'User created, access for this account will be reviewed shortly'
		},
		userUpdateFailed:{
			message: 'Could not update user'
		},
		loggedOutSuccess:{
			message: 'Logged out successfully'
		},
		loginIncorrect:{
			error: true,
			message: 'Incorrect username and/or password'
		},
		loginNotHaveCustNumber:{
			error: true,
			message: 'Sorry, you have not been assigned a customer number yet'
		},
		notHaveAccess:{
			message: 'You do not have access to the RAM Admin Console'
		},
		passwordNotMatch:{
			message: 'Passwords did not match'
		},
		invalidClientName:{
			message: 'Invalid client name'
		},
		userCreatedSuccess:{
			message: 'Successfully created a user'
		},
		userCreatedFailed:{
			message: 'could not create user'
		},
		usernameExist:{
			message: 'username already exist in RAM'
		},
		userAccessUpdateError:{
			message: 'Error updating user'
		},
		userAccessAdded:{
			message: 'Access added to user'
		},
		invalidCustomerId:{
			message: 'Invalid Customer ID'
		},
		alreadyLoggedInAnotherDevice:{
			message: 'Already logged in on another device, please logout and try again.'
		},
		requestDeliver:{
			error: false,
			message: 'Request delievered'
		},
		authenticated:{
			message: 'Unauthorized'
		},
		unAuthenticated:{
			message: 'Unauthorized'
		},
		missingCustomerDetails:{
			error: true,
			message: 'Missing customer details for asset'
		},
		missingAttributes:{
			error: true,
			message: 'Missing attributes for asset'
		},
		restrictedAccess:{
			message: 'Restricted access to asset'
		},
		invalidIdNumber:{
			error: true,
			message: 'Invalid ID Number'
		},
		invalidRFIDNumber:{
			error: true,
			message: 'Invalid RFID Number'
		},
		upgradeVersion:{
			message: 'Please upgrade version of RAM from the app store'
		},
		noAssetHistoryFound:{
			message: 'No asset history found'
		},
		notValidInputFields:{
			error: true,
			message: 'Please check your input fields'
		},
		inspectNoSupported:{
			error: true,
			message: 'No supported G/Y/R selection'
		},
		inspectNoSchedule:{
			error: true,
			message: 'No inspection schedule found for this asset'
		},
		inspectNoListReason:{
			error: true,
			message: 'Could not find listed reason for service'
		},
		inspectUpdateFailed:{
			error: true,
			message: 'Could not update inspection status'
		},
		inspectUpdateSuccess:{
			error: false,
			message: 'Inspection service has been updated'
		},
		inspectError:{
			error: true,
			message: 'Error processing inspection '
		},
		emailSendingError:{
			message: 'Error sending email: '
		},
		meterReadingNotFoundSerial:{
			message: 'Could not find serial number'
		},
		meterReadingNotValidValue:{
			error: true,
			message: 'New meter reading value is invalid'
		},
		meterReadingUpdateFailed:{
			message: 'Could not update meter'
		},
		meterReadingUpdateSuccess:{
			error: false,
			message: 'Meter has been recorded'
		},
		meterReadingError:{
			message: 'Error processing update meter '
		},
		invalidSerialNumber:{
			message: 'Invalid serial number'
		},
		rfidRegisterSuccess:{
			error: false,
			message: 'Successfully register RFID'
		},
		rfidDeregisterSuccess:{
			error: false,
			message: 'Successfully deregister RFID'
		},
		notHaveAccessToView:{
			error: true,
			code: '401',
			message: "You don't have access to view that asset"
		},
		statusMsgGood:{
			message: 'Good'
		},
		statusMsgServiceSoon:{
			message: 'Service Soon'
		},
		statusMsgServiceThisWeek:{
			message: 'Service This Week'
		},
		statusMsgServiceOverdue:{
			message: 'Service Overdue'
		},
		statusMsgUnknown:{
			message: 'Unknown'
		},
		searchMachineNotFound:{
			message: 'Machine Not Found'
		},
		woCreatedSingleFailed:{
			message: ' failed to be created in WO. Message: '
		},
		woError:{
			error: true,
			message: 'Error creating WO: '
		},
		woCreatedSuccess:{
			error: false,
			message: 'Work order has been recorded'
		},
		woNoData:{
			message: 'No data send to server'
		},
		newItemCreatedSuccess:{
			error: false,
			message: 'Asset has been recorded'
		},
        newItemError:{
			error: true,
			message: 'Error creating new asset' 
		},
		newItemCreatedButFailedAttachRFID:{
			error: true,
			message: 'Asset details are created successfully but failed to attach rfid'
		},
		newItemCreatedAssetComponentFailed:{
			error: true,
			message: 'Error creating Asset Component' 
		},
		newItemExcelNotCorrectFormat:{
			error: true,
			message: 'Worksheet is not in correct format'
        },
        newItemExcelNotCorrectDataNumber:{
			error: true,
			message: 'Data number on Asset Details sheet should not be less than data number on Asset Components'
        },
        newItemNotNumber:{
			error: true,
			message: 'Field/column Length should be in number'
        },    
        newItemNotValidLength:{
            error: true,
            message: ' length should not more than '
        }, 
        newItemMissingRequired:{
            error: true,
            message: 'Missing required column (ITEM #, MACHINE ID)'
        }, 
        lotMasterCreatedFailed:{
            error: true,
            message: 'New asset are created successfully but failed to create its LOT master'
        }, 
        serialisedMaintenanceFailed:{
            error: true,
            message: 'New asset are created successfully but failed to create its serialised maintenance'
        }, 
        inspectionScheduleCreatedFailed:{
            error: true,
            message: 'New asset are created successfully but failed to create its inspection schedule'
        }, 
        setMachinePositionAssetCreatedFailed:{
            error: true,
            message: 'New asset are created successfully but failed to set its the machine position'
        },  
        invalidAutonumberConfiguration: {
			error: true,
			message: 'Invalid autonumber configuration'
		},
        autoGenerateNumberFailed:{
            error: true,
            message: 'Could not generate auto number'
        },
        errorOccuredOnRow:{
            error: true,
            message: 'Error occurred on row '
        },
        warningOccuredOnRow:{
            error: true,
            message: 'Warning occurred on row '
        },
		machinePositionUpdated : {
            error : false,
            message : 'Successfully update machine position information'
        },
        rfidAlreadyRegistered : {
            error : true,
            message : 'This RAM1921 RFID is associated with another asset, please deregister it from that asset before registering it to a new asset.'
        },
        newItemCannotBeHigherValue : {
            error: true,
            message: ' value cannot be higher than '
        },
        newItemCannotBeLessValue : {
            error: true,
            message: ' value cannot be less than '
        },
        updateTCSuccess : {
			error: false,
			message: 'The user has been agreed to RYCO Terms and Conditions'
		},
        userHasNotAgreeTermsYet : {
			error: true,
			message: 'Please indicate that you have read and agree to the Terms and Conditions'
		}
	}
}