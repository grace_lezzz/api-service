var queryHelper = require('../sql/queryHelper');
var queries = require('../sql/queries');
var messages = require('./api_responses').get();
var Promise = require('bluebird');
var moment = require('moment');
var connection = require('../databases/db_connection_config.js').getConnRHAUApp();
var sql = require('mssql');


exports.generate = function (id) {
    return new Promise(function (resolve, reject) {
        var transaction = new sql.Transaction(connection);

        transaction.begin(function (err) {
            var request = new sql.Request(transaction);

            request.query(queries.updateLastNumber(id), function (err) {
                if (err) {
                    reject(messages.invalidAutonumberConfiguration);
                } else {
                    request.query(queries.getLatestNumber(id), function (err, result) {
                        if (err) {
                            reject(messages.invalidAutonumberConfiguration);
                        } else {
                            if (result.length == 0) {
                                reject(messages.invalidAutonumberConfiguration);
                            }

                            var numberGenerator = getNumberGenerator(id);
                            numberGenerator(id, result[0].last_number, result[0].key_value, request, function (err, generatedNumber) {
                                transaction.commit(function (err) {
                                    if (err) {
                                        reject(messages.invalidAutonumberConfiguration);
                                        console.log(err.message);
                                    } else {
                                        resolve(generatedNumber);
                                    }
                                });
                            });
                        }
                    });
                }
            })
        });
    });
};

function getNumberGenerator(id) {
    switch (id) {
        case 'LotNumber': return lotNumberGenerator;
        case 'OrderNumber': return orderNumberGenerator;
        default: throw { isError: true, message: messages.invalidAutonumberConfiguration }
    }
}

function lotNumberGenerator(id, latestNumber, keyValue, request, callback) {
    var currentYear = moment().format("YY");

    if (currentYear == keyValue) {
        callback(undefined, 'R' + keyValue + padNumber(latestNumber, 9));
    } else {
        // reset the number
        request.query(queries.resetLastNumberByKeyValue(id, currentYear), function (err) {
            // asumed the new number is now start from 1
            callback(err, 'R' + currentYear + padNumber(1, 9));
        });
    }
}

function orderNumberGenerator(id, latestNumber, keyValue, request, callback) {
    var currentYear = moment().format("YYMMDD");

    if (currentYear == keyValue) {
        callback(undefined, 'WP' + keyValue + padNumber(latestNumber, 7));
    } else {
        // reset the number
        request.query(queries.resetLastNumberByKeyValue(id, currentYear), function (err) {
            // asumed the new number is now start from 1
            callback(err, 'WP' + currentYear + padNumber(1, 7));
        });
    }
}

function padNumber(number, numberOfZero) {
    var pad_char = '0';
    var pad = new Array(1 + numberOfZero).join(pad_char);
    return (pad + number).slice(-pad.length);
} 

