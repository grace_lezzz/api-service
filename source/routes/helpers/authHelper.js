var queryHelper = require('../sql/queryHelper.js');
var queries = require('../sql/queries.js');
var messages = require('./api_responses.js').get();
var sql = require('mssql'); 
var connection = require('../databases/db_connection_config.js').getConnRHAUApp();
var crypto = require('crypto');

// Restrict user to use apikey
exports.restrictByApiKey = function (req, res, next) {
	var apikey = req.headers.apikey;
	
	if(!apikey) {
        apikey = req.body.apikey;
        
        if(!apikey)
		  return res.status(401).send('Unauthorized');
	}
	
	queryHelper.executeQuery(queries.getAccess(apikey), { 
					message: messages.noUserFound,
					isMulti: false,
					auth: true,
					initial: true
	})
	.then(function (data) {
		if(data) {
            // inject customer id data
            req.body.customer_id = data.customer_id;
            req.body.customer_no = data.customer_no;
            req.body.isAdmin = data.RAM_ADMIN;
            req.body.user = data.username;
            
			// Proceed
			next();
		} else {
			return res.status(401).send(messages.unAuthenticated.message);
		}
	})
	.catch(function() {
		return res.status(401).send(messages.unAuthenticated.message);
	});
}

exports.isValidApiKey = function(req, res, next) {
    return res.send("Ok");
}

exports.getCustomerInformation = function (req, res, next) {
    var customer_id = req.body.customer_id;
    var customer_no = req.body.customer_no;
    
    if(customer_id) {
        queryHelper.executeQuery(queries.getCustomerInformation(customer_id, customer_no), {
            isMulti: false
        })
        .then(function(data) {
            req.body.customer_no = data.customer_no;
            req.body.division = data.facility;
            req.body.facility = data.facility;
            
            //for upload
            req.headers.customer_no = data.customer_no;
            req.headers.division = data.facility;
            req.headers.facility = data.facility;
            
            next();
        })
        .catch(function() {
            next();
        })
    }    
}


exports.authenticateUser = function authenticateUser(username, password, ip, callback) {
    // Generate API key
    var salt = crypto.randomBytes(256);
    var apikey = crypto.createHash('md5').update(username + salt).digest('hex'); 

    var request = new sql.Request(connection);
    request.input('username', sql.VarChar(25), username);
    request.input('password',sql.VarChar(25), password);
    request.input('apikey',sql.VarChar(32), apikey);
    request.input('ip_addr',sql.VarChar(32), ip);

    // Now requires an entry in RAMUSERACCESS for authentication, otherwise the user is bounced
    request.execute('RAM_ValidateAndUpdateApiKey', function(err, rows, returnValue) {
        if(err) {
            console.log(err.message);
            return callback(messages.couldNotConnect);
        }
        else {
            if (rows[0][0] == undefined) {
                return callback(messages.loginIncorrect);
            }
            else if (returnValue==-1) {
                return callback( messages.loginNotHaveCustNumber);
            }
            else {
                return callback( {error: false, message: messages.authenticated.message, data: rows} );
            }
        }
    });
}
