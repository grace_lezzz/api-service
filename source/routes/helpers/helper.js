exports.writeJSONError = function (message, res) {
	var result = [];
	result.push({
		error: true,
		message: message
	});
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify(result[0]));
};

exports.validateField = function (username) {
    if (username == undefined) {
        return false;
    }
    if (username.length < 1 || username.length > 128) {
        return false;
    }

    return true;
}

exports.getDateFormat = function (date){
	var dateFormat = date.getFullYear().toString().substring(0,4) + 
					("0" + (date.getMonth() + 1)).slice(-2) +
					("0" + date.getDate()).slice(-2);
	return dateFormat;					
}

exports.getTimeFormat = function (date){
	var timeFormat = ("0" + date.getHours()).slice(-2).toString() + ("0" + date.getMinutes()).slice(-2);
	return timeFormat;
}

exports.tag = Tag;
// TAG class: Strips rfid prefix values and validations
function Tag(id) {
	if (id != undefined && ((id.substring(0,4).toUpperCase().indexOf("E005") > -1) || (id.substring(0,4).toUpperCase().indexOf("E004") > -1))) {
		id = id.toUpperCase();
		id = id.substring(4,id.length).toUpperCase();
	}
	this.id = id;
}
Tag.prototype = {
	isValid: function() {
		if (this.id == undefined) {
			return false;
		}
		if ((this.id.length > 0)&&(this.id.length <= 30)) {
			return true;
		}
		return false;
	}
};

exports.validateHeader = function (header, sheet) {
    for (var index in header) {
        var element = header[index];

        var cell = sheet[element.cell + '1'];

        if (cell === undefined || cell.v != element.value) {
            return false;
        }
    }

    return true;
}

exports.readDataFromSheet = function (header, sheet) {
    var rowIndex = 2;
    var result = [];
    while (true) {
        var currentResult = {};
        var isEmptyLine = true;
        for (var index in header) {
            var element = header[index];

            var cell = sheet[element.cell + rowIndex];
            
            if(cell === undefined)
                currentResult[element.fieldName] = '';
            else{
                currentResult[element.fieldName] = cell.v;
                isEmptyLine = false;
            }
        }
        
        // Stop if there are no value at all on a line
        if(isEmptyLine)
            break;
            
        result.push(currentResult);
        rowIndex++;
    }
    
    return result;
}

exports.getMonthsFromDateString = function(date) {
    var month = date.toString().substring(4,6);
    
    return parseInt(month);
}
