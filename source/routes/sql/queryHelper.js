var messages = require('../helpers/api_responses.js').get();
var Promise = require("bluebird");
var sql = require('mssql');
var connectionConfig = require('../databases/db_connection_config.js')
var connection = connectionConfig.getConnRHAUM3();
var connectionFactoryM3 = connectionConfig.getConnectionFactoryM3();

var db=Promise.promisifyAll(require('mssql'));

/* Promised based database lookup
	 Inputs: SQL statement, error message
	 		Optional: return even if data is missing
	 		Optional: Multi row option if more than one row to be returned
	 		Outputs: Single row only, with errors */
exports.executeQuery = function (statement, options) {
	var initial = options.initial;
    var message = options.message;
    var isMulti = options.isMulti;
    var auth = options.auth;
    var doesNotRequireResponse = options.doesNotRequireResponse;
    
    // Options
	if (!options.initial) 		
		initial = false;
        
	if (!options.isMulti)		
		isMulti = false;
        
	if (!options.auth)		
		auth = false;
        
	if (!options.doesNotRequireResponse)		
		doesNotRequireResponse = false;

	return new Promise(function (resolve, reject) {
		var request = new db.Request(connection);

		request.query(statement, function(err, data) {
			if (err) {
				if(message) {
                    reject(message);
                } else {
                    reject(messages.couldNotConnect);
                } 
                console.log("Database connection error: " + err); 
                return; 
			}
			if (doesNotRequireResponse) { resolve(true); return; }
			if (data == undefined) { reject(messages.couldNotConnect); return; }
			
			if (data[0] == undefined && initial) { reject(message); return; }
            
			// Return records if connection successful
			if (isMulti) {
				resolve(data)
			}
			else {
				resolve(data[0]);
			}
		});
	});
}

exports.enhancedExecuteQuery = function (statement, options) {
	var initial = options.initial;
    var message = options.message;
    var isMulti = options.isMulti;
    var auth = options.auth;
    var doesNotRequireResponse = options.doesNotRequireResponse;
    
    // Options
	if (!options.initial) 		
		initial = false;
        
	if (!options.isMulti)		
		isMulti = false;
        
	if (!options.auth)		
		auth = false;
        
	if (!options.doesNotRequireResponse)		
		doesNotRequireResponse = false;

	return new Promise(function (resolve, reject) {
		var request = new db.Request(connection);

		request.query(statement, function(err, data) {
			if (err) {
                
                if(message) {
                    reject(message);
                } else {
                    reject(messages.couldNotConnect);
                } 
                console.log("Database connection error: " + err); 
                return; 
            }
            
			if (doesNotRequireResponse) { resolve(true); return; }
			if (data == undefined) { reject(messages.couldNotConnect); return; }
			
			if (data[0] == undefined && initial) { reject(message); return; }
            
			// Return records if connection successful
			if (isMulti) {
				resolve(data)
			}
			else {
				resolve(data[0]);
			}
		});
	});
}

exports.executeStoredProcedure = function (procedure, params, customer_no, returnNotFound) {
	var doesNotRequireResponse = returnNotFound.doesNotRequireResponse;
	
	if (!returnNotFound.doesNotRequireResponse)		
		doesNotRequireResponse = false;
	
	return new Promise(function (resolve, reject) {
        var currentConnection = connectionFactoryM3(customer_no); 
		var request = new db.Request(currentConnection);
		for (var x in params) {
			if(params[x].length !== undefined)
				request.input(params[x].name,sql.VarChar(params[x].length), params[x].value);
			else
				request.input(params[x].name,sql.VarChar(32), params[x].value);
		}
		request.execute(procedure, function(err, data) {
			if (err) { console.log(err); reject(messages.couldNotConnect); return; }
			if (doesNotRequireResponse) { resolve(true); return; }
			if (data == undefined) { reject(returnNotFound); return; }
			if (data[0] == undefined) { reject(returnNotFound); return; }
			if (data[0].length == 0) { reject(returnNotFound); return; }
			// Return records if connection successful
			resolve(data[0]);
		});
	});
}