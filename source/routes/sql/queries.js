var dbConfig = require('../databases/db_connection_config');
var dbName = dbConfig.getDBName();
var transQueEnv = dbConfig.getEnvironment();

exports.getAccess = function(id) {
	var query = "select RAM_ADMIN, customer_id, customer_no, username from RHAU_Application.dbo.RAMUSERS a"
	+ " left outer join RHAU_Application.dbo.RAMUSERACCESS b"
	+ " on a.id = b.user_id"
	+ " where apikey = '" + id + "'";
    
    console.log(query);
    
	return query;
}

exports.getCustomerInformation = function(id, customer_no) {
    var query = "select OKCONO as customer_no, OKDIVI as division, OKFACI as facility "
              + "from " + dbName(customer_no) + ".dbo.OCUSMA "
              + "where OKCUNO = '" + id  +"'";
                                
    return query;
}

exports.updateUserCred = function(newPassword, ip, username){
	var query = "UPDATE RHAU_Application.dbo.RAMUSERS SET "
    + "password='" + newPassword + "', "
    + "last_login_ip='" + ip + "', "
    + "first_login='" + 0 + "' "
    + "WHERE username='" + username + "'";
	return query;
}

exports.getUserByUsernameAndApikey = function(apikey, username){
	var query = "SELECT username from RHAU_Application.dbo.RAMUSERS where apikey='" + apikey + "' AND username='" + username + "'"; 
	return query;
}

exports.getUserByUsername = function(username){
	var query = "SELECT username FROM RHAU_Application.dbo.RAMUSERS where username='" + username + "'";
	return query;
}

exports.getUserAccess = function(uid){
	var query = "SELECT * from RHAU_Application.dbo.RAMUSERACCESS where user_id='" + uid  + "'";
	return query;
}


exports.addNewMessage = function(data) {
    var query = "insert into RHAU_Application.dbo.RAM_MESSAGE (Title, ValidFrom, ValidTo, Message) values ('" + data.title + "','" + data.validFrom + "','" + data.validTo + "','" + data.message  +"')";
    
    return query;
}

exports.getMessageByDate = function(date) {
    var query = "select id, Title, ValidFrom, ValidTo from RHAU_Application.dbo.RAM_MESSAGE where ValidFrom <= " + date + " and ValidTo >= " + date + " Order By id Desc";
    
    return query;
}

exports.getMessageById = function(id) {
    var query = "select Message from RHAU_Application.dbo.RAM_MESSAGE where id = " + id;
    
    console.log(query);
    
    return query;
}