// Mailer
var nodemailer = require('nodemailer');
var messages = require('./helpers/api_responses').get();

var SETTINGS_EMAIL_TO_ADMIN = 'STBranigan@RYCO.com.au'; 
var SETTINGS_MAIL_FROM_ACCOUNT_NAME = 'RYCO RAM'
var SETTINGS_EMAIL_FROM = 'ryco.deve@gmail.com';
var SETTINGS_EMAIL_FROM_PASSWORD = 'ryco1921';
var SETTINGS_EMAIL_FROM_SERVICE = 'Gmail';

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: SETTINGS_EMAIL_FROM_SERVICE,
    auth: {
        user: SETTINGS_EMAIL_FROM,
        pass: SETTINGS_EMAIL_FROM_PASSWORD
    }
});

// Email log for recent activity
exports.logEmail = function(subject, content) {
    var mailOptions = {
        from: SETTINGS_MAIL_FROM_ACCOUNT_NAME + ' <' + SETTINGS_EMAIL_FROM + '>', // sender address
        to: SETTINGS_EMAIL_TO_ADMIN, // list of receivers
        subject: subject, // Subject line
        html: content
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(messages.emailSendingError.message + error);
        }
    });
}

// Write email to specific email address
exports.generateEmailTo = function(mailto, subject, content) {
    var mailOptions = {
        from: SETTINGS_MAIL_FROM_ACCOUNT_NAME + ' <' + SETTINGS_EMAIL_FROM + '>', 
        to: mailto,
        subject: subject, 
        html: content
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log(messages.emailSendingError.message + error);
        }
    });
}
// Modified MF 14-10-15
exports.createNewUserEmail = function (username, contactEmail, contactName, company, contactPhone, ip) {
    var subject = "RYCO RAM New User Created for " + contactName;
    var content = "Hi " + contactName + "."
    + "<p><b>Welcome to RYCO RAM.</b>"
    + "<p>Your username is <b>" + username + "</b>"
    // + "<p>You can access the RAM application straight away, but you'll need a RYCO team member to setup you up with access to your assets."
    // + "<p>Having your customer ID number handy will help speed up the proccess, so if you require access please respond to this email."
    + "<p>The contact details you supplied us with are <b>Ph:</b> " + contactPhone + " and <b>Email:</b> " + contactEmail + "."
    + "<p>Not you? Please ignore this email. This account was created from the IP address " + ip + ".";
    
    module.exports.generateEmailTo(contactEmail, subject, content); 
} 

// Added MF 14-10-15
exports.createNewUserEmailToAdmin = function (username, contactEmail, contactName, company, contactPhone, ip) {
    var subject = "Action Required: RYCO RAM New User Created for " + contactName + ".";

    var content = "<p>A new user has been created with the following details."
    + "<p><b> Name: </b>" + contactName
    + "<p><b> Company: </b>" + company
    + "<p><b> Username: </b>" + username
    + "<p><b> Email: </b>" + contactEmail
    + "<p><b> Phone Contact: </b>" + contactPhone
    + "<p><b> IP Address: </b>" + ip
    + "<p>"
    + "<p> Login to the RAM Administrator Portal to provide access to this user."    
    module.exports.generateEmailTo(SETTINGS_EMAIL_TO_ADMIN, subject, content); 
} 

exports.requestAccessTo = function(username, contactName, contactEmail, contactPhone, customerId) {

	var content = "User: <b>" + username + "</b> requires access to an asset with customer id <b>" + customerId + "</b>." 
                + "<p>Contact: " + contactName
                + "<p>Email: " + contactEmail
                + "<p>Phone: " + contactPhone;

    var subject = 'REQUEST FOR ACCESS User: ' + username;

    module.exports.logEmail(subject, content);
}