var messages = require('./helpers/api_responses.js').get();
var queries = require('./sql/queries.js');
var queryHelper = require('./sql/queryHelper.js');
var connection = require('./databases/db_connection_config.js').getConnRHAUApp();
var sql = require('mssql'); 
var helper = require('./helpers/helper.js');
var rfid = require('./RFID.js');
var Promise = require("bluebird");
var XLSX = require('xlsx');
var autoNumberGenerator = require('./helpers/autoNumberGenerator');
var usedHeader = [];

var HEADER = [
    {
        value: 'ITEM #',
        fieldName: 'item'
    },
    {
        value: 'OEM #',
        fieldName: 'oem'
    },
    {
        value: 'MACHINE ID',
        fieldName: 'machineId'
    },
    {
        value: 'DESCRIPTION',
        fieldName: 'description'
    },    
    {
        value: 'HOSE TYPE',
        fieldName: 'hoseType'
    },
    {
        value: 'HOSE LENGTH',
        fieldName: 'hoseLeng'
    },
    {
        value: 'END 1',
        fieldName: 'endOne'
    },
    {
        value: 'END 1 ATTACHMENT',
        fieldName: 'endOneAtt'
    },
    {
        value: 'END 2',
        fieldName: 'endTwo'
    },
    {
        value: 'END 2 ATTACHMENT',
        fieldName: 'endTwoAtt'
    },
    {
        value: 'COVER 1',
        fieldName: 'coverOne'
    },
    {
        value: 'COVER 1 LENGTH',
        fieldName: 'coverOneLeng'
    },
    {
        value: 'COVER 2',
        fieldName: 'coverTwo'
    },
    {
        value: 'COVER 2 LENGTH',
        fieldName: 'coverTwoLeng'
    },
    {
        value: 'COVER 3',
        fieldName: 'coverThree'
    },
    {
        value: 'COVER 3 LENGTH',
        fieldName: 'coverThreeLeng'
    },
    {
        value: 'ORIENTATION',
        fieldName: 'orientation'
    }	
];

exports.addAssetDetails = function(req, res){
    var dataBody = req.body.data;
    var bodyRequest = {};
    bodyRequest.customer_no = req.body.customer_no;
    bodyRequest.facility = req.body.facility;
    bodyRequest.division = req.body.facility;

    dataBody.apikey = req.headers.apikey;
    if (!dataBody.apikey) {
        dataBody.apikey = req.body.apikey;
    }
    
    var resultPromiseForm = [];

    resultPromiseForm.push(addAssetDetail(dataBody, bodyRequest));
    
    Promise.all(resultPromiseForm)
    .then(function(itemMessages){
        if(itemMessages[0].error)
            res.send(itemMessages[0]);
        else
            res.send(messages.newItemCreatedSuccess);
    });		
}

var addAssetDetail = function(dataBody, bodyRequest){
    var customer_no = bodyRequest.customer_no;
    var facility = bodyRequest.facility;
    var resultMessage = {}
    return new Promise( function(resolve, reject){
        //validate required field;
        if(dataBody.mfgPart === '' ||
           dataBody.machineId === '')
        {
            resolve(messages.newItemMissingRequired);
            return;            
        }
        
        //validate column type
        if( typeof dataBody.hoseLeng === 'string' ||
            typeof dataBody.coverOneLeng === 'string' ||
            typeof dataBody.coverTwoLeng === 'string' ||
            typeof dataBody.coverThreeLeng === 'string' ||
            typeof dataBody.orientation === 'string' )
        {
            resolve(messages.newItemNotNumber);
            return;        
        }
        
        //validate orientation column
        var validLimit = validMinMaxValue(dataBody.orientation, 'Orientation', 0, 360);
        if(validLimit.error){
            resolve(validLimit);
            return;
        }
        
        //validate length of each column
        var validateLength = validLength(dataBody); 
        if(validateLength.error)
        {
            resolve(validateLength);
            return;
        }
        
        //validate rfid format
        if(dataBody.rfids.length !== 0 ) {
            for(var i = 0; i< dataBody.rfids.length; i++){
                var currentRFID = dataBody.rfids[i].trim();
                if(currentRFID){
                   if(!rfid.funcValidateRfid(currentRFID, "generated", null))
                    {
                        resolve(messages.invalidRFIDNumber);
                        return;
                    } 
                }
                
            }
        }

        queryHelper.executeQuery(queries.getAccess(dataBody.apikey), { 
            message: messages.noUserFound,
            auth: true,
            initial: true

        })
        .then(function (data){
            if (data === undefined) {
                throw messages.noUserFound;
            }
            
            dataBody.custNum = data.customer_id;
            
            return autoNumberGenerator.generate('OrderNumber');
        })
        .then(function(data) {
            if(data === undefined){
                throw messages.autoGenerateNumberFailed;
            }
            
            dataBody.orderNumber = data;
            
            console.log("order number: " + data);
            
            return autoNumberGenerator.generate('LotNumber');
        })
        .then(function(data){
            if(data === undefined){
                throw messages.autoGenerateNumberFailed;
            }
            dataBody.lotNumber = data;
            
            console.log("lot number: " + data);            
            
         	return queryHelper.executeQuery(queries.getMachinesByMachineId(dataBody.custNum, dataBody.machineId, customer_no),{ message:messages.assetNotFound} );
        })
        .then(function(data){
            if (data === undefined) {
                dataBody.serial = '';
            }
            else{
                dataBody.serial = data.MITAIL;	
            }

            var params = composingParameters(dataBody);
            bodyRequest.orderDate = params.filter(function(v) { return v.name === 'orderDate'; })[0].value;
            bodyRequest.orderTime = params.filter(function(v) { return v.name === 'orderTime'; })[0].value;

            return queryHelper.executeStoredProcedure('RHAU_Application.dbo.sp_RAM_addNewItem', params, customer_no, { message: messages.couldNotConnect, doesNotRequireResponse : true});
        })
        .then(function (data) {
           
            //continue to create asset component
            var component = setAssetComponent(dataBody);
            
            var resultPromise = [];
            var seq = 10;
            for (var property in component) {
                resultPromise.push(addSingleComponent(component, property, seq, dataBody));
                seq = seq + 10;
            }
            
            Promise.all(resultPromise)
            .then(function(itemMessages){
                if(itemMessages[0].error)
                    resolve(itemMessages[0]);
                else
                {
                    var rfidResultPromise = [];
                    //after success execute the SP of add new item, now loop the rfid to attach the rfid to the lot number
                    if(dataBody.rfids.length !== 0 ) {
                        for(var i = 0; i< dataBody.rfids.length; i++) {
                            var currentRFID = dataBody.rfids[i].trim();
                            rfidResultPromise.push(rfid.registerRFID(dataBody.lotNumber, currentRFID, 'HA-RYC-GEN-ITEM'));
                        }
                        return Promise.all(rfidResultPromise);
                                              
                    } else {
                        return [];
                    }
                }
            })
            .then(function(resultMessages) {
                for(var i in resultMessages) {
                    if(resultMessages[i].error) {
                        resultMessage = messages.newItemCreatedButFailedAttachRFID;
                    }
                }
                
                // Create the LOT MASTER entry for the newly installed item
                // This is to create an item in MILOMA to insert into the machine position
                var lotData = {};
                lotData.lot = dataBody.lotNumber;
                lotData.serial = dataBody.serial;
                lotData.ref_ord_no = dataBody.orderNumber;
                lotData.facility = facility;
                lotData.date = bodyRequest.orderDate;
                lotData.time = bodyRequest.orderTime;
                return queryHelper.enhancedExecuteQuery(queries.createLotMaster(lotData, customer_no), {
                    message: messages.lotMasterCreatedFailed, doesNotRequireResponse: true
                });
            })
            .then(function(){
                // Create the serialised maintenance item
                // This is to create a MILOIN (serialised maintenance) item for servicing
                var serialisedItem = {};
                serialisedItem.lot = dataBody.lotNumber;
                serialisedItem.serial = dataBody.serial;
                serialisedItem.customer_id = dataBody.custNum;
                serialisedItem.date = bodyRequest.orderDate;
                serialisedItem.time = bodyRequest.orderTime;
                serialisedItem.item_description = dataBody.desc;
                serialisedItem.machine = dataBody.machineId;
                return queryHelper.enhancedExecuteQuery(queries.createSerialisedMaintenanceItem(serialisedItem, customer_no), {
                    message: messages.serialisedMaintenanceFailed, doesNotRequireResponse: true
                });
            })
            .then(function(){
                // Create inspection schedule for installed asset
                // This is to create a dummy inspection in MWOPLP so that the item shows up in the mobile device when machine is searched
                var inspection = {};
                inspection.lot = dataBody.lotNumber;
                inspection.serial = dataBody.serial;
                inspection.date = bodyRequest.orderDate;
                inspection.time = bodyRequest.orderTime;
                inspection.ref_ord_no = dataBody.orderNumber;
                return queryHelper.enhancedExecuteQuery(queries.createInspectionSchedule(inspection, customer_no, facility), {
                    message: messages.inspectionScheduleCreatedFailed, doesNotRequireResponse: true
                });
            })
            .then(function(){
                resultMessage = messages.newItemCreatedSuccess;
                // Set Machine Position for the new asset
                return queryHelper.enhancedExecuteQuery(queries.setMachinePositionForNewItem(dataBody.serial, dataBody.lotNumber, customer_no), {
                    message: messages.setMachinePositionAssetCreatedFailed, doesNotRequireResponse: true
                });
            })
            .then(function() {
                resolve(resultMessage);
            })
            .catch(function(err) {
                err.error = true;
                console.log(err.message);
                resolve(err);
            });
        })
        .catch(function(err){
            err.error = true;
            console.log(err.message);
            resolve(err);
        });    
    });

}

function validLength(dataBody)
{
    if(dataBody.desc.length > 40 )
        return {error: true, message: HEADER[3].value + messages.newItemNotValidLength.message + '40'};
    if(dataBody.oemPart.length > 15)
        return {error: true, message: HEADER[1].value + messages.newItemNotValidLength.message + '15'};
    if(dataBody.mfgPart.length > 20)
        return {error: true, message: HEADER[0].value + messages.newItemNotValidLength.message + '20'};    
    if(dataBody.machineId.length > 15)
            return {error: true, message: HEADER[2].value + messages.newItemNotValidLength.message + '15'};        
    if(dataBody.hoseType.length > 10)
            return {error: true, message: HEADER[4].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.endOne.length > 10)
            return {error: true, message: HEADER[6].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.endOneAtt.length > 10)
            return {error: true, message: HEADER[7].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.endTwo.length > 10)
            return {error: true, message: HEADER[8].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.endTwoAtt.length > 10)
            return {error: true, message: HEADER[9].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.coverOne.length > 10)
            return {error: true, message: HEADER[10].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.coverTwo.length > 10)
            return {error: true, message: HEADER[12].value + messages.newItemNotValidLength.message + '10'};        
    if(dataBody.coverThree.length > 10)
            return {error: true, message: HEADER[14].value + messages.newItemNotValidLength.message + '10'};
     
    if(dataBody.rfids.length > 0)
    {
        for(var i =0; i < dataBody.rfids.length; i++){
            if(dataBody.rfids[i].length > 25)
                return {error: true, message: 'RFID' + messages.newItemNotValidLength.message + '25'};                
        }
    }
    return {error:false};
}

function validMinMaxValue(value, header, min, limit){
    if(parseInt(value) > limit){
        return {error: true, message: header + messages.newItemCannotBeHigherValue.message + limit};
    }
    if(parseInt(value) < min){
        return {error: true, message: header + messages.newItemCannotBeLessValue.message + min};
    }
    return {error:false};
}

var getEmptyFieldMessage = function(fieldValue, header, row){
    var isEmpty = false;
    if(typeof fieldValue === 'string'){
        if(fieldValue.trim() === ''){
            isEmpty = true;
        }
    } else if(typeof fieldValue === 'number'){
        if(fieldValue === 0){
            isEmpty = true;
        }
    }
    
    if(isEmpty){
        return header;
    }
    return '';
}

var bulkWarningMessage = function(columns, rowIndex){
    var emptyColumns = [];
    for(var i=0; i<columns.length; i++){
        var messageValue = getEmptyFieldMessage(columns[i].value, columns[i].header, rowIndex);
        if(messageValue){
            emptyColumns.push(messageValue);
        }
    }
    
    if(emptyColumns.length > 0){
        var info = messages.warningOccuredOnRow.message + (rowIndex+1) + ': Found empty field ';
        info = info + '(' + emptyColumns.join(', ') + ')';
        return info;
    }
    return '';
}

var columnsToCheck = function(dataBody){
    var columns = [];
    columns.push({ value : dataBody.oemPart, header : 'OEM #' });
    columns.push({ value : dataBody.desc, header : 'Description' });
    columns.push({ value : dataBody.hoseType, header : 'Hose Type' });
    columns.push({ value : dataBody.hoseLeng, header : 'Hose Length' });
    columns.push({ value : dataBody.endOne, header : 'End 1' });
    columns.push({ value : dataBody.endOneAtt, header : 'End 1 Attachment' });
    columns.push({ value : dataBody.endTwo, header : 'End Two' });
    columns.push({ value : dataBody.endTwoAtt, header : 'End Two Attachment' });
    columns.push({ value : dataBody.coverOne, header : 'Cover 1' });
    columns.push({ value : dataBody.coverOneLeng, header : 'Cover 1 Length' });
    columns.push({ value : dataBody.coverTwo, header : 'Cover 2' });
    columns.push({ value : dataBody.coverTwoLeng, header : 'Cover 2 Length' });
    columns.push({ value : dataBody.coverThree, header : 'Cover 3' });
    columns.push({ value : dataBody.coverThreeLeng, header : 'Cover 3 Length' });
    columns.push({ value : dataBody.orientation, header : 'Orientation' });
    
    // iterate RFID columns
    var rfids = dataBody.rfids;
    if(rfids){
        for(var i=0; i<rfids.length; i++){
            columns.push({ value : rfids[i], header : 'RFID ' + (i+1) });
        }
    }
    
    return columns;
}

var composingParameters = function(dataBody){
	var orderDate = helper.getDateFormat(new Date());
	var orderTime = helper.getTimeFormat(new Date());
		
	var params = [];
	params.push({ name: "orderNumber", value: dataBody.orderNumber});
	params.push({ name: "orderDate", value: orderDate});
	params.push({ name: "orderTime", value: orderTime});
	params.push({ name: "customerOrderNumber", value: dataBody.orderNumber});
	params.push({ name: "description", value: dataBody.desc, length: 255});
	params.push({ name: "lotNumber", value: dataBody.lotNumber});
	params.push({ name: "rfid1", value: ''});
	params.push({ name: "rfid2", value: ''});
	params.push({ name: "oemPart", value: dataBody.oemPart});
	params.push({ name: "mfgPart", value: dataBody.mfgPart});
	params.push({ name: "customerNumber", value: dataBody.custNum});
	params.push({ name: "machineID", value: dataBody.machineId});
	params.push({ name: "machineSerialNumber", value: dataBody.serial});

	return params;
}

var setAssetComponent = function(dataBody){
	var component = {};
	component['HOSETYPE'] = dataBody.hoseType;
	component['HOSE-LENG'] = dataBody.hoseLeng;
	component['ENDONE'] = dataBody.endOne;
	component['ENDONEATT'] = dataBody.endOneAtt;
	component['ENDTWO'] = dataBody.endTwo;
	component['ENDTWOATT'] = dataBody.endTwoAtt;	
	component['COVER1'] = dataBody.coverOne;	
	component['COVERE1-LEN'] = dataBody.coverOneLeng;	
	component['COVER2'] = dataBody.coverTwo;	
	component['COVERE2-LEN'] = dataBody.coverTwoLeng;	
	component['COVER3'] = dataBody.coverThree;	
	component['COVERE3-LEN'] = dataBody.coverThreeLeng;
	component['ORIENTATION'] = dataBody.orientation;	
	
	return component;
}

var addSingleComponent = function(component, property, seq, dataBody) {
	
	return new Promise( function(resolve, reject){
		var componentProperty = {};
		componentProperty.lotNumber = dataBody.lotNumber;
		componentProperty.sequence = seq;
		componentProperty.field = property;
		componentProperty.mfg = dataBody.mfgPart;
		if(typeof component[property] === 'string')
		{
			componentProperty.text = component[property];
			componentProperty.number = 0;
		}
		else
		{
			componentProperty.text = '';
			componentProperty.number = component[property];
		}
			
		queryHelper.executeQuery(queries.addAssetComponent(componentProperty), { 
				message: messages.newItemCreatedAssetComponentFailed,
				doesNotRequireResponse: true
			})
		.then( function (data){
			resolve({error:false});
		})
		.catch(function (error) {
			resolve(error);
		});
	});
}

exports.addAssetDetailsByExcel = function(req, res) {
    var bodyRequest = {};
    bodyRequest.customer_no = req.headers.customer_no;
    bodyRequest.facility = req.headers.facility;
    bodyRequest.division = req.headers.facility;
    
    // Read excel file from file
    try{
        var workbook = XLSX.readFile('uploads/' + req.file.filename);
        usedHeader = [];
        if (!isValidFormat(workbook))
        {
            res.send(messages.newItemExcelNotCorrectFormat);
            return;
        }
        
        var assetDetailSheet = workbook.Sheets[workbook.SheetNames[0]];
        var assetDetailData = helper.readDataFromSheet(usedHeader, assetDetailSheet);
       
        var resultPromiseRow = [];
        var totalRow = assetDetailData.length;
        var warningMessages = [];
        for(var i=0; i<totalRow; i++){
            var dataBody = composeDataBody(req, assetDetailData[i]);
            
            // warning - It is not error but only return warning message
            var warningColumns = columnsToCheck(dataBody);            
            warningMessages.push(bulkWarningMessage(warningColumns, i+1));
            
            resultPromiseRow.push(addAssetDetail(dataBody, bodyRequest));
        }
        
        var isError = false;
        
        Promise.all(resultPromiseRow)
        .then(function(itemMessages){
            var errMsg = "";
            var rowIndex = 2;
            itemMessages.forEach(function(message){
                if(message.error)
                {
                    if(isError)
                        errMsg = errMsg + '|' + messages.errorOccuredOnRow.message + rowIndex + ": " + message.message;
                    else
                        errMsg = messages.errorOccuredOnRow.message + rowIndex + ": " + message.message;
                        
                    isError = true;
                }
                rowIndex++;
            });
            
            if(isError){
                res.send({error: isError, message:errMsg});
            }
            else{
                var successMessage = messages.newItemCreatedSuccess.message;
                // add warning message
                if(warningMessages.length > 0){
                    var warnings = warningMessages.join('|');
                    successMessage = successMessage + '|' + warnings;
                }
                res.send({error: false, message:successMessage}); 
            }
        });	
    }catch(exception) {
        console.log(exception);
		res.send(messages.newItemError);
	}
}

function composeDataBody(req, assetDetailData){
    var dataBody = {};
    
    dataBody.apikey = req.headers.apikey;
    if (!dataBody.apikey) {
        dataBody.apikey = req.body.apikey;
    }
    
    if(assetDetailData.description)
        dataBody.desc = assetDetailData.description.toUpperCase().trim();
    else
        dataBody.desc = '';
        
    if(assetDetailData.oem)
        dataBody.oemPart = assetDetailData.oem.toUpperCase().trim();          
    else
        dataBody.oemPart = '';
    
    if(assetDetailData.item)
        dataBody.mfgPart = assetDetailData.item.toUpperCase().trim();
    else
        dataBody.mfgPart = '';
    
    if(assetDetailData.machineId)
        dataBody.machineId = assetDetailData.machineId.toUpperCase().trim();
    else
        dataBody.machineId = '';

    if(assetDetailData.hoseType)
        dataBody.hoseType = assetDetailData.hoseType.toUpperCase().trim();                    
    else
        dataBody.hoseType = '';
        
    assetDetailData.hoseLeng = trimIfString(assetDetailData.hoseLeng);
    if(assetDetailData.hoseLeng)
        dataBody.hoseLeng = assetDetailData.hoseLeng;
    else
        dataBody.hoseLeng = 0;

    if(assetDetailData.endOne)
        dataBody.endOne = assetDetailData.endOne.toUpperCase().trim();
    else
        dataBody.endOne = '';

    if(assetDetailData.endOneAtt)
        dataBody.endOneAtt = assetDetailData.endOneAtt.toUpperCase().trim();
    else
        dataBody.endOneAtt = '';

    if(assetDetailData.endTwo)
        dataBody.endTwo = assetDetailData.endTwo.toUpperCase().trim();
    else
        dataBody.endTwo = '';

    if(assetDetailData.endTwoAtt)
        dataBody.endTwoAtt = assetDetailData.endTwoAtt.toUpperCase().trim();
    else
        dataBody.endTwoAtt = '';

    if(assetDetailData.coverOne)
        dataBody.coverOne = assetDetailData.coverOne.toUpperCase().trim();
    else
        dataBody.coverOne = '';

    assetDetailData.coverOneLeng = trimIfString(assetDetailData.coverOneLeng);
    if(assetDetailData.coverOneLeng)
        dataBody.coverOneLeng = assetDetailData.coverOneLeng;        
    else
        dataBody.coverOneLeng = 0;            

    if(assetDetailData.coverTwo)
        dataBody.coverTwo = assetDetailData.coverTwo.toUpperCase().trim();
    else
        dataBody.coverTwo = '';

    assetDetailData.coverTwoLeng = trimIfString(assetDetailData.coverTwoLeng);
    if(assetDetailData.coverTwoLeng)
        dataBody.coverTwoLeng = assetDetailData.coverTwoLeng;
    else
        dataBody.coverTwoLeng = 0;

    if(assetDetailData.coverThree)
        dataBody.coverThree = assetDetailData.coverThree.toUpperCase().trim();
    else
        dataBody.coverThree = '';

    assetDetailData.coverThreeLeng = trimIfString(assetDetailData.coverThreeLeng);
    if(assetDetailData.coverThreeLeng)
        dataBody.coverThreeLeng = assetDetailData.coverThreeLeng;
    else
        dataBody.coverThreeLeng = 0;
    
    assetDetailData.orientation = trimIfString(assetDetailData.orientation);
    if(assetDetailData.orientation)
        dataBody.orientation = assetDetailData.orientation;
    else
        dataBody.orientation = 0;
    
    dataBody.rfids = [];
    var properties = Object.keys(assetDetailData);
    properties.forEach(function(property){
        if(property.indexOf('rfid') > -1 )
        {
            dataBody.rfids.push(assetDetailData[property].toUpperCase().trim());
        }
    });
    
    return dataBody;
}

function isValidFormat(workbook) {
    // Worksheet must contain at least one sheet
    if (workbook.Sheets.length == 0)
        return false;

    var uploadSheet = workbook.Sheets[workbook.SheetNames[0]];

    // Sheet must have correct header
    if (validateDynamicHeader(HEADER, uploadSheet)) {
        return true;
    }

    return false;
}

function trimIfString(property){
    if(typeof property === 'string')
        return property.trim();
    else
        return property;
}

function validateDynamicHeader(header, sheet) {
    var colIndex = 'A';
    while (true) {
        var cell = sheet[colIndex + '1'];

        if (cell == undefined) {
            break;
        }

        var isColumnValid = false;

        for (var rowIndex in header) {
            var currentHeader = header[rowIndex];

            if (cell.v == currentHeader.value) {
                isColumnValid = true;
                currentHeader.cell = colIndex;
                usedHeader.push(currentHeader);
                break;
            } else if (cell.v.indexOf("RFID") > -1) {
                isColumnValid = true;
                var rfidCell = {
                    value: cell.v,
                    fieldName: cell.v.replace(/\s/g, '').toLowerCase(),
                    cell: colIndex
                };

                usedHeader.push(rfidCell);
                break;
            }
        }

        if (!isColumnValid) {
            return false;
        }

        colIndex = nextChar(colIndex);
    }

    return true;
}

function nextChar(char) {
    return String.fromCharCode(char.charCodeAt(0) + 1);
}