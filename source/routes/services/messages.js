var fs = require('fs');
var path = require('path');
var queryHelper = require('./sql/queryHelper.js');
var connection = require('./databases/db_connection_config.js').getConnRHAUApp();
var queries = require('./sql/queries.js');
var messages = require('./helpers/api_responses.js').get();

exports.uploadMessage = function (req, res) {
    var fileName = req.file.filename;
    var originalname = (req.file.originalname).replace('.pdf', '');
    var title = req.body.title;
    var validFrom = req.body.validFrom;
    var validTo = req.body.validTo;
    
    var uploadPath = './uploads/' + fileName;

    fs.readFile(uploadPath, function (err, data) {
        if(!err) {
            var request = {
                title : title,
                validFrom : validFrom,
                validTo : validTo,
                message : data.toString('base64')
            }
            queryHelper.executeQuery(queries.addNewMessage(request), { message: messages.couldNotConnect, doesNotRequireResponse : true})
            .then(function () {
                res.send("Ok");
            })
            .catch(function() {
                res.status(400).send("bad request");
            })
        }
    });

}

exports.getMessageByDate = function(req, res) {
     var date = req.body.currentDate;
    
    queryHelper.executeQuery(queries.getMessageByDate(date), { message: messages.couldNotConnect, initial: false, isMulti : true})
    .then(function(result) {
        res.send({error: false, data: result});
    })
    .catch(function(err) {
        res.status(400).send("bad request");  
    });
}

exports.getMessageById = function(req, res) {
    var id = req.params.id;
    
    queryHelper.executeQuery(queries.getMessageById(id), { message: messages.couldNotConnect, initial: false, isMulti : false})
    .then(function(result) {
        res.setHeader("Content-Type", "application/pdf");
        var pdf = new Buffer(result.Message, 'base64');
        res.send(pdf);
    })
    .catch(function(err) {
        res.status(400).send("bad request");  
    });
}

exports.getBase64MessageById = function(req, res) {
    var id = req.params.id;
    
    queryHelper.executeQuery(queries.getMessageById(id), { message: messages.couldNotConnect, initial: false, isMulti : false})
    .then(function(result) {
        var pdf = new Buffer(result.Message, 'base64');
        res.send({error: false, data: result.Message});
    })
    .catch(function(err) {
        res.status(400).send("bad request");  
    });
}