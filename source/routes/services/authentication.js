// Changed all SQL Statements to look at the correct database.tables on AWS RDS
// STBranigan - Uploaded to bitbucket repositoty:  12/10/2015

var express = require('express');
var router = express.Router();
var sql = require('mssql'); 
var connection = require('./databases/db_connection_config.js').getConnRHAUApp();
var helper = require('./helpers/helper.js');
var authHelper = require('./helpers/authHelper.js');
var queries = require('./sql/queries.js');
var messages = require('./helpers/api_responses.js').get();
var queryHelper = require('./sql/queryHelper.js');

exports.authenticate = function(req,res) {
    var ip = res.connection.remoteAddress;
    var username = req.body.username;
    var password = req.body.password;

    authHelper.authenticateUser(username, password, ip, function(authenticated) {
        var error = authenticated.error;
        var message = authenticated.message;
        if (error) {
            helper.writeJSONError(message, res);
        }
        else {
            var output = { error: false };
            var data = authenticated.data[0][0];
            output.username = data.username;
            output.apikey = data.apikey;
            output.firstTimeLoggedIn = data.first_login;
            res.send(output);
        }
    });
}

function updateUserCredentials(username, newPassword, ip, callback) {
    var request = new sql.Request(connection);
    
    request.query(queries.updateUserCred(newPassword,ip, username), function (error, records) {
        if (error) {
            return callback({ error: true, message: error});
        }
        else {
            return callback({ error: false });
        }
    });
}

exports.changeUser = function(req, res) {
    var ip = res.connection.remoteAddress;
    var username = req.body.username;
    var newPassword = req.body.newPassword;
    var apikey = req.body.apikey;

    if (!helper.validateField(username)) {
        helper.writeJSONError(messages.invalidUsername.message, res);
        return;
    }

    if (!helper.validateField(apikey)) {
        helper.writeJSONError(messages.invalidApikey.message, res);
        return;
    }

    if (!helper.validateField(newPassword)) {
        helper.writeJSONError(messages.invalidPassword.message, res);
        return;
    }

    var request = new sql.Request(connection);

    request.query(queries.getUserByUsernameAndApikey(apikey, username), function (error, records) {
        if (error) {
            helper.writeJSONError(messages.couldNotConnect.message);
        }
        else {
            if (records.length == 0) {
                helper.writeJSONError(messages.invalidApikey.message, res);
            }
            else {
                updateUserCredentials(username, newPassword, ip, function (callback) {
                    var error = callback.error;
                    if (error) {
                        helper.writeJSONError(messages.userUpdateFailed, res);
                        return;
                    }
                    else {
                        var response = { error: false, username: username, apikey: apikey };
                        res.send(response);
                        return;
                    }
                });
            }
        }
    });
}


exports.login = function(req, res) {
    if (req.session.authenticated) {
        res.render('home', { title: 'Home', id: 'home' })
    }
    else {
        res.render('login', { title: 'Login', id: 'login' })
    }
};

exports.logout = function(req, res) {
    req.session.authenticated = false;
    res.render('login', { title: 'Login', id: 'login', message: messages.loggedOutSuccess.message});
}

exports.loginUser = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;

    var request = new sql.Request(connection);
 
    request.query(queries.getUserLogin(username,password), function (error, records) {
        if (error) {
            helper.writeJSONError(messages.couldNotConnect.message, res);
        }
        else {
            if (records.length == 0) {
                res.render('login', { title: 'Login', id: 'login', message: messages.loginIncorrect.message});
            }
            else {
                var record = records[0];
                if (record.RAM_ADMIN == true) {
                    req.session.authenticated = true;
                    req.session.user = username;
                    res.render('home', { title: 'Home', id: 'home' });
                }
                else {
                    res.render('login', { title: 'Login', id: 'login',  message: messages.notHaveAccess.message });       
                }
            }
        }
    });
};

exports.addUser = function (req, res) {
    res.render('adduser', { title: 'Add user', id: 'adduser' });
}

function createNewUser(name, username, password, callback) {  
    var request = new sql.Request(connection);
 
    request.query(queries.createUser(username, password, name), function (error, records) {
        if (error) {
            return callback(messages.couldNotConnect);
        }
        else {
            return callback({ error: false });
        }
    });

}

exports.createUser = function (req, res) {
    var client = req.body.clientName;
    var password = req.body.password;
    var passwordConfirm = req.body.passwordConfirm;
    var username = req.body.username;

    if (password != passwordConfirm) {
        res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.passwordNotMatch.message});
        return;
    }

    if (!helper.validateField(client)) {
        res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.invalidClientName.message});   
        return;
    }

    if (!helper.validateField(username)) {
        res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.invalidUsername.message});   
        return;
    }

    var request = new sql.Request(connection);

    request.query(queries.getUserByUsername(username), function (error, records) {
        if (error) {
            console.log(error);
            res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.couldNotConnect.message});   
        }
        else {
            if (records.length == 0) {
                createNewUser(client, username, password, function(callback) {
                    var error = callback.error;
                    if (error) {
                        res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.userCreatedFailed.message});   
                    }
                    else {
                        res.render('addUser', { title: 'Add user', id: 'adduser', confirmUserCreated: true, message: messages.userCreatedSuccess.message});   
                    }
                });
            }
            else {
                res.render('addUser', { title: 'Add user', id: 'adduser', message: messages.usernameExist.message});   
            }
        }
    });
}

exports.users = function (req, res) {
    var request = new sql.Request(connection);

    request.query(queries.getAllUser(), function (error, records) {
        if (error) {
            res.render('users', { title: 'Users', id: 'users', message: messages.couldNotConnect.message});   
        }
        else {
            res.render('users', { title: 'Users', id: 'users', users: records });
        }
    });
}

function getUserAccess(uid, callback) {
    var request = new sql.Request(connection);

    request.query(queries.getUserAccess(uid), function (error, records) {
        if (error) {
            return callback({error: true});
        }
        else {
            return callback({ error: false, records: records });
        }
    });
}

exports.user = function (req, res) {
    var uid = req.query.uid;
    var admin = req.query.admin;
    var request = new sql.Request(connection);

    request.query(queries.getUserAccess(uid), function (error, records) {
        if (error) {
            res.render('user', { title: 'User', id: 'user', message: messages.couldNotConnect.message, admin: admin});   
        }
        else {
            res.render('user', { title: 'User', id: 'user', userCustomerIds: records, uid: uid, admin: admin});
        }
    });
}

// TODO: delete
// function getCustomerDetailsFromId(customerId, callback) {
//     var request = new sql.Request(connection);
//     request.query(queries.getCustomerDetailById(customerId), function (error, records) {
//         if (!error && records[0] != undefined && records[0].OKCUNM != undefined) {
//             return callback({error: false, customer: records[0].OKCUNM.trim()})
//         }
//         return callback({error: true});
//     });
// }

exports.addCustomerId = function (req, res) {
    var uid = req.body.uid;
    var customerId = req.body.customerId;
    var userCustomerIds = req.body.userCustomerIds;

    if (customerId.toUpperCase() == "RYCO") {
        var request = new sql.Request(connection);

        request.query(queries.setUserToAdmin(uid), function (error, records) {
            if (error) {
                res.render('user', { title: 'User', id: 'user', userCustomerIds: userCustomerIds, uid: uid, message: messages.userAccessUpdateError.message });  
            }
            else {
                res.render('user', { title: 'User', id: 'user', admin: true, uid: uid, message: messages.userAccessAdded.message, success: true });
            }
        });
    }
    else if (customerId.toUpperCase() == "REMOVE") {
        var request = new sql.Request(connection);

        request.query(queries.removeUserAdmin(uid), function (error, records) {
            if (error) {
                res.render('user', { title: 'User', id: 'user', userCustomerIds: userCustomerIds, uid: uid, message: messages.userAccessUpdateError.message });  
            }
            else {
                res.render('user', { title: 'User', id: 'user', admin: false, uid: uid, message: messages.userAccessAdded.message, success: true });
            }
        });
    }
    else {
        var request = new sql.Request(connection);
        
        request.query(queries.addUserAccess(uid, customerId), function (error, records) {
            if (error) {
                res.render('user', { title: 'User', id: 'user', userCustomerIds: userCustomerIds, uid: uid, message: messages.userAccessUpdateError.message });  
            }
            else {
                getUserAccess(uid, function (callback) {
                    var error = callback.error;
                    var rows = callback.records;

                    if (error) {
                        res.render('user', { title: 'User', id: 'user', userCustomerIds: userCustomerIds, uid: uid, message: messages.userAccessUpdateError.message });  
                    }
                    else {
                        res.render('user', { title: 'User', id: 'user', userCustomerIds: rows, uid: uid, message: messages.userAccessAdded.message, success: true });
                    }
                });
            }
        });
    }
}

exports.removeAccess = function(req, res) {
    var uid = req.query.uid;
    var customer_id = req.query.customer_id;

    var request = new sql.Request(connection);

    request.query(queries.removeUserAccess(uid, customer_id), function (error, records) {
        if (error) {
            res.render('home', { title: 'Home', id: 'home', message: messages.userAccessUpdateError.message });
        }
        else {
            getUserAccess(uid, function (callback) {
                var error = callback.error;
                var rows = callback.records;

                if (error) {
                    res.render('home', { title: 'Home', id: 'home', message: messages.userAccessUpdateError.message });
                }
                else {
                    res.render('user', { title: 'User', id: 'user', userCustomerIds: rows, uid: uid, message: messages.userAccessAdded.message, success: true });
                }
            });
        }
    });
}

function getFormatedDate(now){
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + (month) + (day);
    return today;
}

function getFormatedTime(now){
    var hh = ("0" + now.getHours()).slice(-2);
    var mm = ("0" + (now.getMinutes())).slice(-2);
    var ss = ("0" + (now.getSeconds())).slice(-2);
    var time = (hh) + (mm) + (ss);
    return time;
}

exports.postTermsAgreement = function(req, res) {
    var body = req.body;
    var username = body.username;
    var apikey = req.headers.apikey;
    
    queryHelper.executeQuery(queries.getAccess(apikey), { 
		message: messages.noUserFound,
		isMulti: false,
		auth: true,
		initial: true
	})
    .then(function(access){
        if(access === undefined){
            throw {message: messages.noUserFound};
        }
        var data = {};
        data.terms_ip = res.connection.remoteAddress;
        data.username = username;
        data.terms_id = body.terms_id;
        
        var now = new Date();
        data.terms_date = getFormatedDate(now);
        data.terms_time = getFormatedTime(now);
        
        return queryHelper.executeQuery(queries.updateUserTermsAgreement(data), { 
            message: messages.couldNotConnect, isMulti: false, initial: true, doesNotRequireResponse: true }); 
    })
    .then(function(){
        res.send(messages.updateTCSuccess);
    })
    .catch(function (error) {
        res.send(messages.couldNotConnect);
    })
}

exports.getTermsCondition = function(req, res) {
    queryHelper.executeQuery(queries.getTermAndCondition(), { 
        message: messages.couldNotConnect,
        isMulti: false, 
        initial: true,
        auth: false,
    })
    .then(function (data) {
        res.send({error: false, data: data });
    })
    .catch(function (error) {
        res.send(error);
    });
}

exports.getUserTermsCondition = function(req, res) {
    var username = req.body.username;
    queryHelper.executeQuery(queries.getUserTermsCondition(username), { 
		message: messages.noUserFound,
		isMulti: false, 
        initial: true,
        auth: false
	})
    .then(function (data) {
        if(!data.id){
            res.send({error: false, data: {} });
        } else {
            res.send({error: false, data: data });
        }
    })
    .catch(function (error) {
        res.send(error);
    });
}

exports.validateTermsCondition = function(req, res) {
    var body = req.body;
    var username = body.username;
    var apikey = req.headers.apikey;
    var latestVersion = {};
    var userData = {};
    
    queryHelper.executeQuery(queries.getAccess(apikey), { 
		message: messages.noUserFound,
		isMulti: false,
		auth: true,
		initial: true
	})
    .then(function(data){
        if(data === undefined){
            throw {message: messages.noUserFound};
        }
        // latest terms version
        return queryHelper.executeQuery(queries.getTermAndCondition(), { 
            message: messages.couldNotConnect, isMulti: false, initial: false }); 
    })
    .then(function(data) {
        if(data) latestVersion = data;
        // user's terms version
        return queryHelper.executeQuery(queries.getTermVersionByUsername(username), { 
            message: messages.couldNotConnect, isMulti: false, initial: false }); 
    })
    .then(function(data) {
        userData = data;
        
        if(latestVersion.id == userData.terms_id){
            res.send({error: false, data:{}});
        } else {
            res.send({error: false, data: latestVersion});
        }
    })
    .catch(function(error) {
        res.send({error: true, message: messages.noDataSendToServer.message });
    });
}

exports.validateUser = function (req, res) {
    var dataBody = {};
    dataBody.isAdmin = req.body.isAdmin;
    dataBody.user = req.body.user;
    res.send({error: true, data: dataBody});
}