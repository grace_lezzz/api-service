CREATE TABLE QDB_USERACCESS(
	id INT NOT NULL identity(1,1),
	[username] [varchar](25) NOT NULL,
	[password] [varchar](25) NOT NULL,
	[apikey] [varchar](32) NULL,
	[last_login_ip] [varchar](25) NULL,
	[last_login_date] [varchar](25) NULL,
	[email] [nvarchar](50) NULL,
	[contact_name] [nvarchar](50) NULL,
	[contact_phone] [nvarchar](32) NULL,
	[first_login] [bit] NOT NULL,
	[sector] [nvarchar](30) NULL,
	[terms_date] [numeric](8, 0) NULL,
	[terms_time] [numeric](6, 0) NULL,
	[terms_id] [int] NULL,
	[terms_ip] [nvarchar](25) NULL,
	[qdb_admin] [bit] NOT NULL,
)

CREATE TABLE QDB_ACCESS(
	id INT NOT NULL identity(1,1)
	, access nvarchar(15)
)

CREATE TABLE SECTOR(
	id INT NOT NULL identity(1,1)
	, code nvarchar(15)
	, [description] nvarchar(50)
)

CREATE TABLE STUDENT(
	code varchar(50) NOT NULL PRIMARY KEY
	, name nvarchar(50)
	, [address] nvarchar(200)
	, birth_date numeric(8,0)
	, email nvarchar(50)
)

CREATE TABLE TEACHER(
	code varchar(50) NOT NULL PRIMARY KEY
	, name nvarchar(50)
	, [address] nvarchar(200)
	, birth_date numeric(8,0)
	, email nvarchar(50)
)

CREATE TABLE SUBJECTS(
	id INT NOT NULL identity(1,1)
	, title nvarchar(50)
	, [description] nvarchar(max)
)

CREATE TABLE COMPETENCY(
	id INT NOT NULL identity(1,1)
	, subject_id nvarchar(50)
	, sub_title nvarchar(200)
	, [description] nvarchar(max)
)

CREATE TABLE TEACHER_COMPETENCY(
	id INT NOT NULL identity(1,1)
	, teacher_code varchar(50)
	, competency_id INT
	, [description] nvarchar(max)
)

CREATE TABLE ROOM(
	id INT NOT NULL identity(1,1)
	, max_capacity int
	, location nvarchar(20)
)

CREATE TABLE GRADE(
	id INT NOT NULL identity(1,1)
	, code varchar(20)
	, [description] nvarchar(50)
)

CREATE TABLE CLASS(
	id INT NOT NULL identity(1,1)
	, room_id int
	, [start_date] numeric(8,0)
	, [finish_date] numeric(8,0)
	, grade_id int
	, capacity int
)

CREATE TABLE CLASS_PARTICIPANT(
	id INT NOT NULL identity(1,1)
	, class_id int
	, student_code varchar(50)
	, student_start_date numeric(8,0)
	, student_finish_date numeric(8,0)
	, [active] [bit] NOT NULL
)

CREATE TABLE SUPERVISION(
	id INT NOT NULL identity(1,1)
	, client_code varchar(50)
	, supervisor_code varchar(50)
	, relation varchar(20)
)

CREATE TABLE REPORT_ABSENCE(
	id INT NOT NULL identity(1,1)
	, code varchar(50)
	, [status] varchar(50)
	, absence_date numeric(8,0)
	, absence_hour varchar(10)
)

CREATE TABLE QDB_MESSAGE(
	id INT NOT NULL identity(1,1),
	[valid_from] [numeric](8, 0) NULL,
	[valid_to] [numeric](8, 0) NULL,
	[message] [varchar](max) NULL,
	[title] [varchar](50) NULL,
	[file_name] [varchar](50) NULL,
	[pdf] [bit] NOT NULL,
	[access] [varchar](20) NULL,
	[sender] [varchar](25) NULL
)